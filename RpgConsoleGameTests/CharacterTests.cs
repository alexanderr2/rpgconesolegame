﻿using RpgConsoleGame.Attributes;
using RpgConsoleGame.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RpgConsoleGameTests
{
    public class CharacterTests
    {


        [Fact]
        public void Test_CharacterIsLevelOne()
        {
            Mage mage = new("Mage");
            int expected = 1;
            int actual = mage.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test_LevelTwoWhenLevelUp()
        {
            Mage mage = new("Mage");
            int expected = 2;
            mage.CharacterLevelUp(1);
            int actual = mage.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test_MageCharacterStartsWithCorrectStats()
        {
            Mage mage = new("Mage");
            PrimaryAttributes expected = new() { Strenght = 1, Dexterity = 1, Intelligence = 8 };
            PrimaryAttributes actual = mage.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Test_RangerCharacterStartsWithCorrectStats()
        {
            Ranger ranger = new("Ranger");
            PrimaryAttributes expected = new() { Strenght = 1, Dexterity = 7, Intelligence = 1 };
            PrimaryAttributes actual = ranger.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Test_RogueCharacterStartsWithCorrectStats()
        {
            Rogue rogue = new("Rogue");
            PrimaryAttributes expected = new() { Strenght = 2, Dexterity = 6, Intelligence = 1 };
            PrimaryAttributes actual = rogue.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Test_WarriorCharacterStartsWithCorrectStats()
        {
            Warrior warrior = new("Warrior");
            PrimaryAttributes expected = new() { Strenght = 5, Dexterity = 2, Intelligence = 1 };
            PrimaryAttributes actual = warrior.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test_MageAttributesIncreaseWhenLevelUp()
        {
            Mage mage = new("Mage");
            PrimaryAttributes expected = new() { Strenght = 2, Dexterity = 2, Intelligence = 13 };
            mage.CharacterLevelUp(1);
            PrimaryAttributes actual = mage.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Test_RangerAttributesIncreaseWhenLevelUp()
        {
            Ranger ranger = new("Ranger");
            PrimaryAttributes expected = new() { Strenght = 2, Dexterity = 12, Intelligence = 2 };
            ranger.CharacterLevelUp(1);
            PrimaryAttributes actual = ranger.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Test_RogueAttributesIncreaseWhenLevelUp()
        {
            Rogue rogue = new("Rogue");
            PrimaryAttributes expected = new() { Strenght = 3, Dexterity = 10, Intelligence = 2 };
            rogue.CharacterLevelUp(1);
            PrimaryAttributes actual = rogue.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Test_WarriorAttributesIncreaseWhenLevelUp()
        {
            Warrior warrior = new("Warrior");
            PrimaryAttributes expected = new() { Strenght = 8, Dexterity = 4, Intelligence = 2 };
            warrior.CharacterLevelUp(1);
            PrimaryAttributes actual = warrior.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }
    }
}
