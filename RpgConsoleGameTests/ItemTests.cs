﻿using RpgConsoleGame;
using RpgConsoleGame.Attributes;
using RpgConsoleGame.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RpgConsoleGameTests
{
    public class ItemTests
    {


        Character newCharacter;

   
        [Fact]
        public void Test_EquipWeapon()
        {
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                RequiredLevel = 2,
                EquipmentSlot = Slot.WEAPON_SLOT,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            newCharacter = new Warrior("Lars");
            newCharacter.Equip(testAxe);
        }

        [Fact]
        public void Test_EquipArmor()
        {
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                RequiredLevel = 2,
                EquipmentSlot = Slot.BODY_SLOT,
                ArmorType = ArmorType.PLATE_ARMOR,
                Attributes = new PrimaryAttributes() { Strenght = 1 }
            };
            newCharacter = new Warrior("Lars");
            newCharacter.Equip(testPlateBody);
        }

        [Fact]
        public void Test_WrongWeaponType()
        {
            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                RequiredLevel = 1,
                EquipmentSlot = Slot.WEAPON_SLOT,
                WeaponType = WeaponType.WEAPON_BOW,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };
            newCharacter = new Warrior("Lars");
            newCharacter.Equip(testBow);
        }

        [Fact]
        public void Tets_WrongArmorType()
        {
            Armor testClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                RequiredLevel = 1,
                EquipmentSlot = Slot.HEAD_SLOT,
                ArmorType = ArmorType.CLOTH_ARMOR,
                Attributes = new PrimaryAttributes() { Intelligence = 5 }
            };
            newCharacter = new Warrior("Lars");
            newCharacter.Equip(testClothHead);
        }

        [Fact]
        public void Test_EquipValidWeapon()
        {
            Weapon testSword = new Weapon()
            {
                ItemName = "Awesome Sword",
                RequiredLevel = 1,
                EquipmentSlot = Slot.WEAPON_SLOT,
                WeaponType = WeaponType.WEAPON_SWORD,
                WeaponAttributes = new WeaponAttributes() { Damage = 15, AttackSpeed = 0.8 }
            };
            newCharacter = new Warrior("Lars");
            string expected = "New weapon have been equipped!";
            string actual = newCharacter.Equip(testSword);
            newCharacter.Equip(testSword);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test_EquipValidArmor()
        {
            Armor testPlateHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                RequiredLevel = 1,
                EquipmentSlot = Slot.HEAD_SLOT,
                ArmorType = ArmorType.PLATE_ARMOR,
                Attributes = new PrimaryAttributes() { Strenght = 1 }
            };
            newCharacter = new Warrior("Lars");
            string expected = "New armor have been equipped!";
            string actual = newCharacter.Equip(testPlateHead);
            newCharacter.Equip(testPlateHead);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test_DmgWithNoWeapon()
        {
            Warrior warr = new("Warrior");
            
            double expected = 1 * (1 + (5 / 100));
            double actual = warr.CalculateDamagePerSecond();
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test_DamageWithValidWeapon()
        {
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                RequiredLevel = 1,
                EquipmentSlot = Slot.WEAPON_SLOT,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            newCharacter = new Warrior("Lars");
            newCharacter.Equip(testAxe);

            double expected = (7 * 1.1)*(1 + (5 / 100));
            double actual = newCharacter.CalculateDamagePerSecond();
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test_DamageWithWeaponAndArmor()
        {
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                RequiredLevel = 1,
                EquipmentSlot = Slot.WEAPON_SLOT,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                RequiredLevel = 2,
                EquipmentSlot = Slot.BODY_SLOT,
                ArmorType = ArmorType.PLATE_ARMOR,
                Attributes = new PrimaryAttributes() { Strenght = 1 }
            };
            newCharacter.Equip(testAxe);
            newCharacter.Equip(testPlateBody);
            newCharacter.CalculateArmorAttributeIncrease();

            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));
            double actual = newCharacter.CalculateDamagePerSecond();

            Assert.Equal(expected, actual);


        }

    }
}
