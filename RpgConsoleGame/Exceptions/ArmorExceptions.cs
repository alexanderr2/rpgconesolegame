﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame.Exceptions
{
    class ArmorExceptions : Exception
    {
        public ArmorExceptions()
        {
        }

        public ArmorExceptions(string message) : base(message)
        {
        }

        public override string Message => "Invalid Armor";
    }
}
