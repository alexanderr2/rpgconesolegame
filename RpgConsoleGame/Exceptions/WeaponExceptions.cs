﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame.Exceptions
{
    class WeaponExceptions : Exception
    {
        public WeaponExceptions()
        {
        }

        public WeaponExceptions(string message) : base(message)
        {
        }

        public override string Message => "Invalid Weapon";
    }
}
