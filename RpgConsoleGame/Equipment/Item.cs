﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame
{

    /// <summary>
    /// List of the Slots
    /// </summary>
    public enum Slot
    {
        HEAD_SLOT,
        BODY_SLOT,
        LEG_SLOT,
        WEAPON_SLOT
    }


    /// <summary>
    /// Defines base of an item
    /// </summary>
    public abstract class Item
    {
        public string ItemName { get; set; }
        public int RequiredLevel { get; set; }
        public Slot EquipmentSlot { get; set; }


        public abstract string ItemDescription();
    }
}
