﻿using RpgConsoleGame.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame
{

    /// <summary>
    /// Lit of armor types
    /// </summary>
    public enum ArmorType
    {
        CLOTH_ARMOR,
        LEATHER_ARMOR,
        MAIL_ARMOR,
        PLATE_ARMOR
    }

    /// <summary>
    /// Defines an item of type armor
    /// </summary>
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public PrimaryAttributes Attributes { get; set; }
        
        public override string ItemDescription()
        {
            return $"Armor of type {ArmorType}";
        }
    }
}
