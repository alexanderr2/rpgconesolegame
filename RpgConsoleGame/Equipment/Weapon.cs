﻿using RpgConsoleGame.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame
{
    /// <summary>
    /// List of all weapon types
    /// </summary>
    public enum WeaponType
    {
        WEAPON_AXE,
        WEAPON_BOW,
        WEAPON_DAGGER,
        WEAPON_HAMMER,
        WEAPON_STAFF,
        WEAPON_SWORD,
        WEAPON_WAND
    }


    /// <summary>
    /// Defines an item of type weapon
    /// </summary>
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }

        public override string ItemDescription()
        {
            return $"Weapon of type {WeaponType}";
        }

    }
}
