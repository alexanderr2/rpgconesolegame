﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame.Attributes
{

    /// <summary>
    /// Parent class for primary attributes of characters
    /// </summary>
    public class PrimaryAttributes
    {
        public int Strenght { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

    /// <summary>
    /// Checks if two PrimaryAttributes are the same
    /// </summary>
    /// <param name="obj">Object to compare with</param>
    /// <returns>True if equals otherwise false</returns>
        public override bool Equals(object obj)
        {
            return obj is PrimaryAttributes attributes &&
                Strenght == attributes.Strenght &&
                Dexterity == attributes.Dexterity &&
                Intelligence == attributes.Intelligence;
        }

        /// <summary>
        ///     Adds two PrimaryAttributes togeather
        /// </summary>
        /// <param name="a">Object one</param>
        /// <param name="b">Object two</param>
        /// <returns></returns>
        public static PrimaryAttributes operator +(PrimaryAttributes a, PrimaryAttributes b) => new()
        {
            Strenght = a.Strenght + b.Strenght,
            Dexterity = a.Dexterity + b.Dexterity,
            Intelligence = a.Intelligence + b.Intelligence
        };

    }
}
