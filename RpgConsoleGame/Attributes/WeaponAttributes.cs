﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame.Attributes
{

    /// <summary>
    /// Parent class for weapon attribues
    /// </summary>
    public class WeaponAttributes
    {
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }
    }
}
