﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame
{
    public class GameUI
    {
        Character playerChar;

        // Read Line shortcut
        public static string ReadLine()
        {
            return Console.ReadLine();
        }
        
        

        // Clear screen shortcut
        public static void ClearScreen()
        {
            Console.Clear();
        }


        #region Block of code for different console.writelines
        /// <summary>
        /// Prints out the first message a player sees when starting the game
        /// </summary>
        public static void StarterMessage()
        {
            Console.WriteLine("\nGreetings adventurer!\n");
        }

        /// <summary>
        /// Prints out the last message a player sees when ending the game
        /// </summary>
        public static void EndingMessage()
        {
            Console.WriteLine("\nThank you for your bravoury, until next time!");
        }

        /// <summary>
        /// Askes the player for a number between two specific values
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public static void AskForNumber(int from, int to)
        {
            Console.WriteLine($"\nPlease type a number between {from} and {to}");
        }


        /// <summary>
        /// Askes the player to press a key to continue
        /// </summary>
        public static void PressKeyToContinue()
        {
            Console.WriteLine("Press a key of your choice to continue in the game!");
            Console.ReadLine();
            ClearScreen();
        }

        /// <summary>
        /// Askes the player to enter the name for their character
        /// </summary>
        public static void AskForCharacterName()
        {
            Console.WriteLine("\nPlease enter your characters name: ");
        }

        /// <summary>
        /// Error message if the character name is too long
        /// </summary>
        /// <param name="length"></param>
        public static void CharacterNameErrorMessage(int length)
        {
            Console.WriteLine($"\nName cant be more then {length} letters!");
        }

        /// <summary>
        /// Prints message for a level up
        /// </summary>
        /// <param name="level"></param>
        public static void DisplayLevelUpMessage(int level)
        {
            Console.WriteLine($"\nLevel Up!! New level is {level}.");
        }
        
        /// <summary>
        /// Menu for the different classes in the game
        /// </summary>
        public static void DisplayCharacterSelection()
        {
            Console.WriteLine("Pick a class you want to play");
            Console.WriteLine("1) Mage");
            Console.WriteLine("2) Ranger");
            Console.WriteLine("3) Rogue");
            Console.WriteLine("4) Warrior");
        }

        /// <summary>
        /// Menu for the different options a player can make once a character has been choosen
        /// </summary>
        public static void DisplayMenuOptions()
        {
            Console.WriteLine("Please choose your next action!");
            Console.WriteLine("1) Level up!");
            Console.WriteLine("2) Equip item");
            Console.WriteLine("3) Display character stats");
            Console.WriteLine("4) Exit game");
        }


        #endregion


        #region Block of code for the logic behind the game

        /// <summary>
        /// Askes the player for the character name
        /// </summary>
        /// <param name="characterNameMaxLength"></param>
        /// <returns>String with the character name</returns>
        public static string GetCharacterName(int characterNameMaxLength)
        {
            AskForCharacterName();
            string name = ReadLine();

            while (!ValidNameInput(name, characterNameMaxLength))
            {
                ClearScreen();
                CharacterNameErrorMessage(characterNameMaxLength);
                AskForCharacterName();
                name = ReadLine();
            }

            ClearScreen();
            return name;
        }

        /// <summary>
        /// Validation for the name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="characterMaxLength"></param>
        /// <returns>True if name not empty and contains letters and smaller then 30 characters</returns>
        private static bool ValidNameInput(string name, int characterMaxLength)
        {
            bool nameIsNotEmpty = name != "";
            bool validName = name.Any(c => char.IsLetter(c));
            bool validLength = name.Length < characterMaxLength;

            return nameIsNotEmpty && validName && validLength;
        }

        /// <summary>
        /// Asks player what character to choose
        /// </summary>
        /// <returns>Character type number</returns>
        public static int GetCharacterType()
        {
            DisplayCharacterSelection();
            var inputType = ReadLine();
            int characterType;

            while (!int.TryParse(inputType, out characterType) || characterType < 1 || characterType > 4)
            {
                ClearScreen();
                AskForNumber(1, 4);
                DisplayCharacterSelection();
                inputType = ReadLine();
            }

            ClearScreen();
            return characterType;
        }

        /// <summary>
        /// Asks player what option to choose after creating a character
        /// </summary>
        /// <returns>Action number</returns>
        public static int GetGameAction()
        {
            DisplayMenuOptions();
            var inputAction = ReadLine();
            int action;

            while (!int.TryParse(inputAction, out action) || action < 1 || action > 4)
            {
                ClearScreen();
                AskForNumber(1, 4);
                DisplayMenuOptions();
                inputAction = ReadLine();
            }

            ClearScreen();
            return action;
        }



        #endregion


        #region Code ment for the Equip item option in the menu, not finished...
        //public static int GetEquipment()
        //{
        //    DisplayEquipmentSlots();
        //    var inputType = ReadLine();
        //    int equipType;

        //    while (int.TryParse(inputType, out equipType) || equipType < 1 || equipType > 4)
        //    {
        //        DisplayEquipmentType();
        //        AskForNumber(1, 4);
        //        inputType = ReadLine();
                
        //    }
        //    ClearScreen();
        //    return equipType;
        //}

        //public static int GetSpecificEquipment()
        //{
        //    DisplayRandomItem();
        //    var inputType = ReadLine();
        //    int specificType;

        //    while (int.TryParse(inputType, out specificType) || specificType < 1 || specificType > 4)
        //    {
        //        DisplayRandomItem();
        //        AskForNumber(1, 4);
        //        inputType = ReadLine();

        //    }
        //    ClearScreen();
        //    return specificType;
        //}
        //public static void DisplayRandomItem()
        //{
        //    Console.WriteLine(Game.GetRandomItem());
        //}


        //public static void DisplayEquipmentType()
        //{
        //    Console.WriteLine("Choose the type of armor you want");
        //    Console.WriteLine("1) Cloth");
        //    Console.WriteLine("2) Leather");
        //    Console.WriteLine("3) Mail");
        //    Console.WriteLine("4) Plate");
        //}

        //public static void DisplayEquipmentSlots()
        //{
        //    ClearScreen();
        //    Console.WriteLine("Please choose a slot for the item you want to equip!");
        //    Console.WriteLine("1) Head");
        //    Console.WriteLine("2) Body");
        //    Console.WriteLine("3) Leg");
        //    Console.WriteLine("4) Weapon");
        //}

        //public static void SelectArmorSlotMenu(int action)
        //{
        //    switch (action)
        //    {
        //        case 1:
        //            DisplayEquipmentType();
        //            break;
        //        case 2:
        //            DisplayEquipmentType();
        //            break;
        //        case 3:
        //            DisplayEquipmentType();
        //            break;
        //        case 4:
        //            DisplayEquipmentType();
        //            break;
        //    }
        //}
        #endregion
    }
}

