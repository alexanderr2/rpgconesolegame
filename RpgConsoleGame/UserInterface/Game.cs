﻿using RpgConsoleGame.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame
{
    public class Game
    {
        Character PlayerChar;
        private const int CharacterNameMaxLength = 30;

        /// <summary>
        /// Starter function
        /// </summary>
        public void PlayGame()
        {
            GameUI.StarterMessage();

            int characterType = GameUI.GetCharacterType();
            string name = GameUI.GetCharacterName(CharacterNameMaxLength);
            PlayerChar = CreateCharacter(characterType, name);

            int action;
            do
            {
                action = GameUI.GetGameAction();
            }
            while (PlayGameAction(action));
        }

        /// <summary>
        /// Calls the methods the player chooses in the menu
        /// </summary>
        /// <param name="action">Action entered by player</param>
        /// <returns>False if game is ended oterhwise true</returns>
        private bool PlayGameAction(int action)
        {
            switch (action)
            {
                case 1:
                    PlayerChar.CharacterLevelUp(PlayerChar.Level + 1);
                    break;
                case 2:
                    //GameUI.GetSpecificEquipment();
                    break;
                case 3:
                    PlayerChar.DisplayStats();
                    break;
                default:
                case 4:
                    GameUI.EndingMessage();
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Creation of the player character
        /// </summary>
        /// <param name="charType">Type of character</param>
        /// <param name="name">Character name</param>
        /// <returns>Character that was created</returns>
        public static Character CreateCharacter(int charType = 1, string name = "Jonas Deg")
        {
            switch (charType)
            {
                default:
                case 1:
                    return new Mage(name);
                case 2:
                    return new Ranger(name);
                case 3:
                    return new Rogue(name);
                case 4:
                    return new Warrior(name);
            }
        }

    }
}
