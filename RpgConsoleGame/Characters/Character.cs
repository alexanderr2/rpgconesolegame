﻿using RpgConsoleGame.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame
{
    /// <summary>
    /// Parent class of all the characters
    /// </summary>
    public abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }
        public Dictionary<Slot, Item> Equipment { get; set; }
        public double DPS { get; set; }

        
        /// <summary>
        /// Initializes a character
        /// </summary>
        /// <param name="name">Character name</param>
        /// <param name="strenght">Character strength</param>
        /// <param name="dexterity">Character dexterity</param>
        /// <param name="intelligence">Character intelligence</param>
        public Character(string name, int strenght, int dexterity, int intelligence)
        {
            Name = name;
            Level = 1;
            Equipment = new Dictionary<Slot, Item>();
            BasePrimaryAttributes = new PrimaryAttributes()
            {
                Strenght = strenght,
                Dexterity = dexterity,
                Intelligence = intelligence
            };
            CalculateTotalAttributes();
        }

        /// <summary>
        /// Levels up the character
        /// </summary>
        /// <param name="level">Number of level ups</param>
        public abstract void CharacterLevelUp(int level);

        /// <summary>
        /// Calculates the damage per second
        /// </summary>
        /// <returns>Double with damage per second</returns>
        public abstract double CalculateDps();

        /// <summary>
        /// Equips a armor
        /// </summary>
        /// <param name="armor">Armor object</param>
        /// <returns>String with success message or exception</returns>
        public abstract string Equip(Armor armor);

        /// <summary>
        /// Equips a weapon
        /// </summary>
        /// <param name="weapon">Weapon object</param>
        /// <returns>String with success message or exception</returns>
        public abstract string Equip(Weapon weapon);

        /// <summary>
        /// Calculates and displays character stats
        /// </summary>
        public void DisplayStats()
        {
            CalculateTotalAttributes();
            DisplayCharAttributes(Name, Level, TotalPrimaryAttributes, DPS);

        }


        /// <summary>
        /// Calculates total attributes from base stats and equipped items
        /// </summary>
        public void CalculateTotalAttributes()
        {
            TotalPrimaryAttributes = CalculateArmorAttributeIncrease();
            DPS = CalculateDps();
        }

        /// <summary>
        /// Calculates the damage per second of a weapon
        /// </summary>
        /// <returns></returns>
        public double CalculateDamagePerSecond()
        {
            Item WeaponEquipment;
            bool hasWeaponEquipped = Equipment.TryGetValue(Slot.WEAPON_SLOT, out WeaponEquipment);
            if (hasWeaponEquipped)
            {
                Weapon weapon = (Weapon)WeaponEquipment;
                return weapon.WeaponAttributes.AttackSpeed * weapon.WeaponAttributes.Damage;
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// Calculates the bonuses from armor
        /// </summary>
        /// <returns>new PrimaryAttribute</returns>
        public PrimaryAttributes CalculateArmorAttributeIncrease()
        {
            PrimaryAttributes bonusArmorValues = new() { Strenght = 0, Dexterity = 0, Intelligence = 0 };

            bool hasBodyEquipment = Equipment.TryGetValue(Slot.BODY_SLOT, out Item bodyArmor);
            bool hasLegEquipment = Equipment.TryGetValue(Slot.LEG_SLOT, out Item legArmor);
            bool hasHeadEquipment = Equipment.TryGetValue(Slot.HEAD_SLOT, out Item headArmor);

            if (hasBodyEquipment)
            {
                Armor armor = (Armor)bodyArmor;
                bonusArmorValues += new PrimaryAttributes() { Strenght = armor.Attributes.Strenght, Dexterity = armor.Attributes.Dexterity, Intelligence = armor.Attributes.Intelligence };
            }
            if (hasLegEquipment)
            {
                Armor armor = (Armor)legArmor;
                bonusArmorValues += new PrimaryAttributes() { Strenght = armor.Attributes.Strenght, Dexterity = armor.Attributes.Dexterity, Intelligence = armor.Attributes.Intelligence };
            }
            if (hasHeadEquipment)
            {
                Armor armor = (Armor)headArmor;
                bonusArmorValues += new PrimaryAttributes() { Strenght = armor.Attributes.Strenght, Dexterity = armor.Attributes.Dexterity, Intelligence = armor.Attributes.Intelligence };
            }

            return BasePrimaryAttributes + bonusArmorValues;
        }

        public void DisplayCharAttributes(string name, int level, PrimaryAttributes totalPrimaryAttributes, double dps)
        {
            Console.WriteLine($"Name: {name}\n");
            Console.WriteLine($"Level: {level}\n");
            Console.WriteLine($"Strength: {totalPrimaryAttributes.Strenght}\n");
            Console.WriteLine($"Dexterity: {totalPrimaryAttributes.Dexterity}\n");
            Console.WriteLine($"Intelligence: {totalPrimaryAttributes.Intelligence}\n");
            Console.WriteLine($"Dps: {dps}\n");

        }

    }
}
