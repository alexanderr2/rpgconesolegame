﻿using RpgConsoleGame.Attributes;
using RpgConsoleGame.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame.Characters
{
    public class Rogue : Character
    {
        /// <summary>
        /// Initializes a Rogue character
        /// </summary>
        /// <param name="name">Character name</param>
        public Rogue(string name) : base(name, 2, 6, 1)
        {
        }

        /// <inheritdoc/>
        public override void CharacterLevelUp(int level)
        {
            if (level < 1) throw new ArgumentException();
            PrimaryAttributes levelUpValues = new() { Strenght = 1, Dexterity = 4, Intelligence = 1 };
            BasePrimaryAttributes += levelUpValues;
            Level += 1;
            CalculateTotalAttributes();
        }

        /// <inheritdoc/>
        public override double CalculateDps()
        {
            TotalPrimaryAttributes = CalculateArmorAttributeIncrease();
            double weaponDPS = CalculateDamagePerSecond();
            if (weaponDPS == 1)
            {
                return 1;
            }

            double multiplier = 1 + TotalPrimaryAttributes.Dexterity / 100.0;
            return weaponDPS * multiplier;
        }

        /// <inheritdoc/>
        public override string Equip(Armor armor)
        {
            if (armor.RequiredLevel > Level)
            {
                throw new ArmorExceptions("Your character is too low level to equip this armor");
            }
            if (armor.ArmorType != ArmorType.LEATHER_ARMOR && armor.ArmorType != ArmorType.MAIL_ARMOR)
            {
                throw new ArmorExceptions("Rogue can only equip Leather and Mail armor");
            }
            Equipment[armor.EquipmentSlot] = armor;
            return "New armor have been equipped!";
        }

        /// <inheritdoc/>
        public override string Equip(Weapon weapon)
        {
            if (weapon.RequiredLevel > Level)
            {
                throw new WeaponExceptions("Your character is too low level to equip this weapon");
            }
            if (weapon.WeaponType != WeaponType.WEAPON_DAGGER && weapon.WeaponType != WeaponType.WEAPON_SWORD)
            {
                throw new WeaponExceptions("Rogue can only equip Dagger and Swords!");
            }
            Equipment[weapon.EquipmentSlot] = weapon;
            return "New weapon have been equipped!";
        }
    }
}
