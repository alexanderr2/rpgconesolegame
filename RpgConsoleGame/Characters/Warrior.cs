﻿using RpgConsoleGame.Attributes;
using RpgConsoleGame.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame.Characters
{
    public class Warrior : Character
    {
        /// <summary>
        /// Initailizes a Warrior character
        /// </summary>
        /// <param name="name">Character name</param>
        public Warrior(string name) : base(name, 5, 2, 1)
        {
        }

        /// <inheritdoc/>
        public override void CharacterLevelUp(int level)
        {
            if (level < 1) throw new ArgumentException();
            PrimaryAttributes levelUpValues = new() { Strenght = 3, Dexterity = 2, Intelligence = 1 };
            BasePrimaryAttributes += levelUpValues;
            Level += 1;
            CalculateTotalAttributes();
        }

        /// <inheritdoc/>
        public override double CalculateDps()
        {
            TotalPrimaryAttributes = CalculateArmorAttributeIncrease();
            double weaponDPS = CalculateDamagePerSecond();
            if (weaponDPS == 1)
            {
                return 1;
            }

            double multiplier = 1 + TotalPrimaryAttributes.Strenght / 100.0;
            return weaponDPS * multiplier;
        }

        /// <inheritdoc/>
        public override string Equip(Armor armor)
        {
            if (armor.RequiredLevel > Level)
            {
                throw new ArmorExceptions("Your character is too low level to equip this armor");
            }
            if (armor.ArmorType != ArmorType.PLATE_ARMOR)
            {
                throw new ArmorExceptions("Warrior can only equip Plate armor");
            }
            Equipment[armor.EquipmentSlot] = armor;
            return "New armor have been equipped!";
        }

        /// <inheritdoc/>
        public override string Equip(Weapon weapon)
        {
            if (weapon.RequiredLevel > Level)
            {
                throw new WeaponExceptions("Your character is too low level to equip this weapon");
            }
            if (weapon.WeaponType != WeaponType.WEAPON_SWORD && weapon.WeaponType != WeaponType.WEAPON_HAMMER && weapon.WeaponType != WeaponType.WEAPON_AXE)
            {
                throw new WeaponExceptions("Warrior can only equip Sword, Hammer and Axes!");
            }
            Equipment[weapon.EquipmentSlot] = weapon;
            return "New weapon have been equipped!";
        }
    }
}

