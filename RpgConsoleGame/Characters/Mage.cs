﻿using RpgConsoleGame.Attributes;
using RpgConsoleGame.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgConsoleGame.Characters
{
    public class Mage : Character
    {
        /// <summary>
        /// Initializes a Mage character
        /// </summary>
        /// <param name="name"></param>
        public Mage(string name) : base(name, 1, 1, 8)
        {
        }

        /// <inheritdoc/>
        public override void CharacterLevelUp(int level)
        {
            if (level < 1) throw new ArgumentException();
            PrimaryAttributes levelUpValues = new() { Strenght = 1, Dexterity = 1, Intelligence = 5 };
            BasePrimaryAttributes += levelUpValues;
            Level += 1;
            CalculateTotalAttributes();
        }

        /// <inheritdoc/>
        public override double CalculateDps()
        {
            TotalPrimaryAttributes = CalculateArmorAttributeIncrease();
            double weaponDPS = CalculateDamagePerSecond();
            if (weaponDPS == 1)
            {
                return 1;
            }

            double multiplier = 1 + TotalPrimaryAttributes.Intelligence / 100.0;
            return weaponDPS * multiplier;
        }

        /// <inheritdoc/>
        public override string Equip(Armor armor)
        {
            if (armor.RequiredLevel > Level)
            {
                throw new ArmorExceptions("Your character is too low level to equip this armor");
            }
            if (armor.ArmorType != ArmorType.CLOTH_ARMOR)
            {
                throw new ArmorExceptions("Mages can only equip Cloth armor");
            }
            Equipment[armor.EquipmentSlot] = armor;
            return "New armor have been equipped!";
        }

        /// <inheritdoc/>
        public override string Equip(Weapon weapon)
        {
            if (weapon.RequiredLevel > Level)
            {
                throw new WeaponExceptions("Your character is too low level to equip this weapon");
            }
            if (weapon.WeaponType != WeaponType.WEAPON_STAFF && weapon.WeaponType != WeaponType.WEAPON_WAND)
            {
                throw new WeaponExceptions("Mage can only equip Staffs and Wands");
            }
            Equipment[weapon.EquipmentSlot] = weapon;
            return "New weapon have been equipped!";
        }

    }
}
